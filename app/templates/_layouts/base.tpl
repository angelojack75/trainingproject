<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    {* Title, description, keywords *}
    {block 'seo'}
        {render_meta:raw}
    {/block}

    <link rel="stylesheet" href="{$.assets_public_path('main.css', 'frontend')}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    {* Another head information *}
    {block 'head'}{/block}
</head>
<body>
    {*Я хедер. Меня вставили в базовый шаблон*}
    {include '/_parts/_header.tpl'}

    {block 'content'}
        Прочти меня! (выше кстатии хедер вставлен)
        Этот текст не виден потому что блок content перекрыт в твоей странице index которая открывается когда ты заходишь на сайт.

        Для нас точкой входа на сайт является страница index ее содержимое в файле index.tpl в этом проекте-примере.
        Шаблонизатор html страниц смотрит в index.tpl когда ты входишь на 0.0.0.0:9000/ = корень сайта  и видит: ( открой и изучи app/templates/pages/index/index.tpl)

        Продолжение... там


    {/block}


    {block 'core_js'}
        <script src="{$.assets_public_path('vendors.js', 'frontend')}"></script>
        <script src="{$.assets_public_path('main.js', 'frontend')}"></script>
    {/block}
    {render_dependencies_js:raw}
    {render_inline_js:raw}
    {render_dependencies_css:raw}
    {render_inline_css:raw}
    {block 'js'}

    {/block}
{if $.is_debug}<div id="DEBUG"></div>{/if}
</body>
</html>